/* 
The following program create an array to store all
the winning app of the MTN Business App of the year award
since 2012. 
a. Sort and print app my name
b. Print the winning app of 2017 and 2018.
c. Print the total number of app from the array 
*/

void main() {
  var winningApp = [
    "fnb-banking",
    "bookly",
    "live-inspect",
    "cput-mobile",
    "domestly",
    "orderin",
    "cowa-bunga",
    "digger",
    "checkers-sixty",
    "ambani-afrika",
  ];

//b. Print the winning app of 2017
  print("This winning app for 2017 is ${winningApp[5]}");

//Print the winning app for 2018.
  print("This winning app for 2018 is ${winningApp[6]}");

// a. Sort and print app my name
  print("The sorted list of winning App is shown below:");
  sortWinningApp(winningApp);

//c. Print the total number of app from the array */
  print("The total number of winning App is ${winningApp.length}");
}

void sortWinningApp(var winningApp) {
  winningApp.sort();
  print(winningApp);
}
