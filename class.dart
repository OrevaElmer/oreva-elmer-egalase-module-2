/*
Create a class and 
a. then use an object to print:
  the name of the app, sector /category, developer, year it won MTN Business App of the year Awards

b. create a function inside the class,transform the app name to all capital letters
  print the output.
*/
void main() {
  var ambani = new WinningApp();

  ambani.appName = "Ambani-Africa";
  ambani.appCategory = "Education";
  ambani.appDeveloper = "Mukundi Lambani";
  ambani.appYear = 2021;

  ambani.printWinningAppDetails();
}

class WinningApp {
  String? appName;
  String? appCategory;
  String? appDeveloper;
  int? appYear;

  void printWinningAppDetails() {
    print("The winning App is ${appName?.toUpperCase()}.");
    print("It belongs to $appCategory category.");
    print("$appDeveloper developed it in the year 2018.");
    print(
        "It was the winning App for the MTN business app in the year $appYear.");
  }
}
