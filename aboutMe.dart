/* 
Write a basic program that store and then print the following data:
a. Your name.
b. Favourite App
c. City 
*/
void main() {
  String myName = "Egalase Oreva Elmer";
  String myFavouriteApp = "WhatsApp";
  var myCity = "Warri";

  printDetails(myName, myFavouriteApp, myCity);
}

void printDetails(String myName, String myFavouriteApp, var myCity) {
  print("My name is $myName");
  print("I frequently check $myFavouriteApp daily");
  print("I am from $myCity, Delta State, Nigeria");
}
